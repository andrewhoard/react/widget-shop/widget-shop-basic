import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import rootReducer from './reducers/index';

let logger = createLogger({
    collapsed: true
})

let store = createStore(
    rootReducer,
    applyMiddleware(logger)
);

export default store;

