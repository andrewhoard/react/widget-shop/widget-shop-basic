/**
 * Combines all the reducers and exposes them as one data structure.
 */

import { combineReducers } from 'redux';

import basket from './basket';

export default combineReducers({
    basket: basket
})
