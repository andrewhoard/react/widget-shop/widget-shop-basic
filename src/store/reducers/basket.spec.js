import cart from "./basket";

describe("reducers", () => {
    describe("basket", () => {
        const initialState = {
            basketTotal: 0,
            widgetIds: []
        };

        it("should provide the initial state", () => {
            expect(cart(undefined, {})).toEqual(initialState);
        });
    });
});
