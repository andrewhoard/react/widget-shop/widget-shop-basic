import { ActionTypes as types } from '../../actionTypes';

let initialState = {
    basketTotal: 0.00,
    widgetIds: []
};

function basket(state = initialState, action) {

    switch(action.type) {
        case (types.CHANGE_BASKET_TOTAL):

            console.log('In change stock');
            console.log(state);
            let newTotal = state.basketTotal + parseFloat(action.data.widget.price);
            let widgetId = action.data.widget.id;
            return {
                ...state,
                basketTotal: newTotal,
                widgetIds: widgetId
            }
        case (types.ADD_TO_BASKET):
            break
        default:
            return state;
    }
}

export default basket;
