import React from 'react';
import logo from './logo.png';

const PageHeader = (props) => (
    <header className="row">
        <div className="col-md-2">
            <img src={logo} className="logo" alt="logo" />
        </div>
        <div className="col-md-10 mt-2 subtitle">
                {props.subtitle}
        </div>
    </header>
);

export default PageHeader;