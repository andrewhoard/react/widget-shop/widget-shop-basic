import React, { Component } from 'react';
import { connect } from "react-redux";

import './main-page.scss';
import PageHeader from './page-header';
import MainWidgetPanel from './main-widget-panel';
import Search from '../search'
import Basket from '../basket/index'

class App extends Component {

  state = {
      basketTotal: '0.00'
  }

  componentDidMount() {
    this.fetchWidgets();
  }

  fetchWidgets = () => {
    fetch('/widgets.json')
    .then(rsp => rsp.json())
    .then(allWidgets => {
      this.allWidgets = allWidgets;
      this.setState( {widget: this.allWidgets } );
    })
  }

  render() {
    return (
      <div className="container">
          <PageHeader subtitle="Selling the best widgets at competitive prices"/>
          <Basket total={this.props.basketTotal} />
          <Search />
          <MainWidgetPanel widget={this.state.widget} />
      </div>
    );
  }
}

export default connect ((state, props) => {

    return {
        basketTotal: state.basket.basketTotal
    }

})(App);
