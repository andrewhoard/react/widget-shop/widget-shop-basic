import React from 'react';
import Widget from '../widget';

const MainWidgetPanel = (props) => {

    function WidgetList(props) {

        //  Loop for all the widgets
        const widgets = props.widgets.map((widget) =>

            <Widget widget={widget} key={widget.id}/>

        );
        return (
            <div>{widgets}</div>
        );
    }

    if (props.widget) return (
        <div>
            <div className="row mainBanner">
                <h3 className="col-md-12 text-center">
                    Widget
                </h3>
            </div>
            <WidgetList widgets={props.widget}/>
        </div>)
    return (<div>No widgets selected</div>)
}

export default MainWidgetPanel;
