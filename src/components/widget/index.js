import React, { Component } from 'react';
import { ActionTypes as types } from '../../actionTypes'
import "./widget.scss";
import {connect} from "react-redux";

class Widget extends Component {

    constructor(props) {
        super(props);

        this.addWidgetToBasket = this.addWidgetToBasket.bind(this);

    }
    addWidgetToBasket(event) {

        let price = parseFloat(this.props.widget.price);
        this.props.dispatch({type:"CHANGE_BASKET_TOTAL", data: {widget: this.props.widget} });
    }
    render() {

        const widget = this.props.widget;

        return (
        <div>
            <div className="row">
                <h3 className="col-md-12">
                    {widget.title}
                </h3>
            </div>
            <div className="row">
                <div className="col-md-6">
                    <img src={`./${widget.image}`} alt="Widgets" />
                </div>
                <div className="col-md-4">
                    {widget.description}
                </div>
                <div className="col-md-1">
                    &pound;{widget.price}
                </div>
                <div className="col-md-1">
                    <button onClick={this.addWidgetToBasket} className="btn-primary">Add</button>
                </div>
            </div>
        </div>
        )
    }
}

export default connect ((state, props) => {
    return {
        total: state.basketTotal
    }

})(Widget);
