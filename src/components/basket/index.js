import React from 'react';
import './basket.scss';

const Basket = (props) => {

    if (props.total !== undefined) {

        return (
            <div className="basket-container">
                <div>Total &pound;{(props.total).toFixed(2)}</div>
            </div>
        )
    }
    return (
        <div className="basket-container">
            <div>Total &pound;0.00</div>
        </div>
    )
}

export default Basket;
