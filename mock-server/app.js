let express = require('express');
let path = require('path');
let logger = require('morgan');
let http = require('http');
//let routes = require('./routes/index');

let port = 8080;

let app = express();

let server = http.createServer(app);
server.listen(port);
server.on('listening', function() {
    console.log('listening on port ' + port);
})

server.on('error', function(error) {
    switch (error.code) {
        case 'EACCES':
            console.error('Port ' + port + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error('Port ' + port + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
});


module.exports = app;
